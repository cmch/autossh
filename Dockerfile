FROM alpine:edge

RUN apk --no-cache add \
openssh \
socat \
autossh \
netcat-openbsd
